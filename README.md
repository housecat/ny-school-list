# NY School List

Swift application for search and display NY School district data

Application implements RxSwift with dependency through the SPM. 

Details: 
- App uses pagination to load schools to main screen
- GPA is displayed whenever it is available
- Details screen provides map location of the school if available 
