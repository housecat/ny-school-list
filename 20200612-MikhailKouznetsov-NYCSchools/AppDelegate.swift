//
//  AppDelegate.swift
//  20200612-MikhailKouznetsov-NYCSchools
//
//  Created by Mikhail Kouznetsov on 6/12/20.
//  Copyright © 2020 Mikhail Kouznetsov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        let schoolsInteractor = SchoolsInteractor(networkService: SchoolDataService())
        let schoolsViewController = SchoolsViewController(interactor: schoolsInteractor)
        let navigationController = UINavigationController(rootViewController: schoolsViewController)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()

        return true
    }
}

