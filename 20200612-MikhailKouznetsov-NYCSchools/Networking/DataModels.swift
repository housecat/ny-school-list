//
//  DataModels.swift
//  20200612-MikhailKouznetsov-NYCSchools
//
//  Created by Mikhail Kouznetsov on 6/12/20.
//  Copyright © 2020 Mikhail Kouznetsov. All rights reserved.
//

import Foundation

// MARK: - School
struct School: Codable {
    let dbn, schoolName, description: String?
    let primaryAddressLine1, city, zip, stateCode: String?
    let latitude, longitude: String?
    let website, schoolEmail, phoneNumber:String?
    
    var gpa:GPAElement?
    
    enum CodingKeys: String, CodingKey {
        case dbn, city, zip, website, latitude, longitude
        case schoolName = "school_name"
        case description = "overview_paragraph"
        case phoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case primaryAddressLine1 = "primary_address_line_1"
        case stateCode = "state_code"
    }
}

typealias Schools = [School]

struct GPAElement: Codable {
    let satMathAvgScore: String?
    let satWritingAvgScore: String?
    let numOfSatTestTakers: String?
    let satCriticalReadingAvgScore: String?
    
    enum CodingKeys: String, CodingKey {
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}

typealias Gpa = [GPAElement]
