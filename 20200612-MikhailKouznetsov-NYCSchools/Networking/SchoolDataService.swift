//
//  SchoolDataService.swift
//  20200612-MikhailKouznetsov-NYCSchools
//
//  Created by Mikhail Kouznetsov on 6/12/20.
//  Copyright © 2020 Mikhail Kouznetsov. All rights reserved.
//

import Foundation
import RxSwift

struct SchoolEndpoint {
    static let authToken = "mcnkenhttGm00JI4YIp9bjCwo"
    
    private static let baseString:String = "https://data.cityofnewyork.us/resource"
    private static let schoolPath = "/s3k6-pzi2.json"
    private static let gpaPath = "/f9bf-2cp4.json"

    private static var allSchoolsURL:URL? {
        return URL(string: String(format: baseString + "%@", schoolPath))
    }
    
    private static var gpaURL:URL? {
        return URL(string: String(format: baseString + "%@", gpaPath))
    }
    
    static func schoolsURL(from:Int, to:Int) -> URL? {
        var urlComponents = URLComponents()
        let offset = URLQueryItem(name: "$offset", value: String(from))
        let limit = URLQueryItem(name: "$limit", value: String(to - from))
        urlComponents.queryItems = [offset, limit ]
        return urlComponents.url(relativeTo: allSchoolsURL)
    }
    
    static func gpaURL(for school:School) -> URL?{
        var urlComponents = URLComponents()
        let dbn = URLQueryItem(name: "dbn", value: school.dbn)
        urlComponents.queryItems = [dbn]
        return urlComponents.url(relativeTo: gpaURL)
    }
}

protocol SchoolDataServiceable {
    /*
     * Fetches schools from API, paginated
     */
    func fetchSchools(from:Int, to:Int) -> Observable<Schools>
    /*
     * Fetches GPA for school from API that match the search term
     */
    func fetchGPA(for school:School) -> Observable<[GPAElement]>
}

final class SchoolDataService: SchoolDataServiceable {
    
    let session = URLSession(configuration: .default)
    
    func fetchSchools(from:Int, to:Int) -> Observable<Schools>{
        guard let allSchoolsURL = SchoolEndpoint.schoolsURL(from: from, to: to) else { return Observable.empty() }
        return execute(url: allSchoolsURL)
    }
    
    func fetchGPA(for school:School) -> Observable<[GPAElement]>{
        guard let gpaURL = SchoolEndpoint.gpaURL(for: school) else { return Observable.empty() }
        return execute(url: gpaURL)
    }
    
    private func execute<T:Decodable>(url:URL) -> Observable<T>{
        return Observable.create { observer -> Disposable in
            var request = URLRequest(url: url)
            request.addValue(SchoolEndpoint.authToken, forHTTPHeaderField: "X-Auth-Token")
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else { return }
                do{
                    let decoded = try JSONDecoder().decode(T.self, from: data)
                    observer.onNext(decoded)
                } catch (let decodeError) {
                    observer.onError(decodeError)
                }
                observer.onCompleted()
            }

            task.resume()
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
}

