//
//  DetailsViewController.swift
//  20200612-MikhailKouznetsov-NYCSchools
//
//  Created by Mikhail Kouznetsov on 6/12/20.
//  Copyright © 2020 Mikhail Kouznetsov. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import RxCocoa

final class DetailsViewController: UIViewController {
    
    private var interactor:DetailsInteractor
    private var currentTab:DetailsViewMenu?
    private var disposeBag = DisposeBag()

    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [mapView, tableView])
        stackView.alignment = .fill
        stackView.distribution = .fill
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private let mapView:MKMapView = {
        let mapView = MKMapView(frame: .zero)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        return mapView
    }()
    
    private lazy var menu:MKTableMenuView = {
        let menu = MKTableMenuView(height: 50, scrollable: true)
        menu.delegate = self
        return menu
    }()
    
    private lazy var tableView:UITableView = {
        let tableView = UITableView(frame: .zero, style: UITableView.Style.plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "DetailCell")
        tableView.register(LocationCell.self, forCellReuseIdentifier: "LocationCell")
        tableView.bounces = false
        tableView.tableHeaderView = menu
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        return tableView
    }()
    
    override var title : String? {
        set {
            super.title = newValue
            configureTitleView()
        }
        get {
            return super.title
        }
    }
    
    init( with school:School) {
        self.interactor = DetailsInteractor(networkService: SchoolDataService(), school: school)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        initMapWihLocation()
        setupLayout()
        setupBinds()
    }
    
    func setupBinds(){
        interactor
            .schoolStream
            .observeOn(MainScheduler.instance)
            .asObservable().subscribe(onNext: { school in
                if school.gpa != nil{
                    self.menu.buidMenu([DetailsViewMenu.description.rawValue,
                                        DetailsViewMenu.location.rawValue,
                                        DetailsViewMenu.gpa.rawValue])
                } else {
                    self.menu.buidMenu([DetailsViewMenu.description.rawValue,
                                        DetailsViewMenu.location.rawValue])
                }
            }).disposed(by: disposeBag)
        
        interactor
            .detailsDataSource
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: tableView.rx.items) { tableView, row, element in
                switch self.currentTab! {
                case .description, .gpa:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCell")!
                    cell.textLabel?.numberOfLines = 0
                    cell.textLabel?.text = element
                    return cell
                case .location:
                    let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell") as! LocationCell
                    cell.textView.text = element
                    return cell
                }
        }.disposed(by: disposeBag)
    }
}

private extension DetailsViewController{
    func setupNavigation(){
        title = interactor.getSchoolName()
    }
    
    func setupLayout() {
        view.backgroundColor = .systemBackground
        stackView.frame = view.bounds
        view.addSubview(stackView)

        let guide = view.safeAreaLayoutGuide
        stackView.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: guide.bottomAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: guide.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: guide.trailingAnchor).isActive = true
        
        mapView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.30).isActive = true
        mapView.isHidden = !interactor.hasLocationData()
    }
    
   func configureTitleView() {
        let someLargeNumber:CGFloat = 4096
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: someLargeNumber, height: someLargeNumber))
        titleLabel.numberOfLines = 0
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        style.tailIndent = -56
    
        let attrText = NSAttributedString(string: title!,
                                          attributes: [NSAttributedString.Key.paragraphStyle : style,
                                                       NSAttributedString.Key.foregroundColor : UIColor.darkGray,
                                                       NSAttributedString.Key.font : UIFont.systemFont(ofSize: 18,
                                                                                                       weight: .semibold)])
        titleLabel.attributedText = attrText
        navigationItem.titleView = titleLabel
    }
    
    func initMapWihLocation(){
        guard let center = interactor.getSchoolLocationCoordinates() else {
            return
        }
        
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        let annotation = MKPointAnnotation()
        annotation.title = interactor.getSchoolName()
        annotation.coordinate = center
        
        mapView.setRegion(region, animated: false)
        mapView.addAnnotation(annotation)
    }
}

extension DetailsViewController:MKTableMenuViewDelegate{
    func menuTabSelected(_ sender: UIButton) {
        currentTab = DetailsViewMenu(rawValue: sender.titleLabel!.text!.lowercased())
        interactor.currentMenuItem(item: currentTab!)
    }
}
