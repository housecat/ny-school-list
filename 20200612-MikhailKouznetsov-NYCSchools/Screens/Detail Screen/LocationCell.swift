//
//  LocationCell.swift
//  20200612-MikhailKouznetsov-NYCSchools
//
//  Created by Mikhail Kouznetsov on 6/14/20.
//  Copyright © 2020 Mikhail Kouznetsov. All rights reserved.
//

import Foundation
import UIKit

class LocationCell:UITableViewCell{
    let textView:UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.regular)
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.dataDetectorTypes = UIDataDetectorTypes.all
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(textView)
        
        textView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        textView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        textView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant:16).isActive = true
        textView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant:-16).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
