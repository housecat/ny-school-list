//
//  DetailsInteractor.swift
//  20200612-MikhailKouznetsov-NYCSchools
//
//  Created by Mikhail Kouznetsov on 6/12/20.
//  Copyright © 2020 Mikhail Kouznetsov. All rights reserved.
//

import MapKit
import Foundation
import RxSwift
import RxCocoa

public enum DetailsViewMenu:String{
    case description = "description"
    case gpa = "gpa"
    case location = "location info"
}


protocol DetailsInteractable{
    func getSchoolLocationCoordinates() -> CLLocationCoordinate2D?
    func getSchoolName() -> String?
    func hasLocationData() -> Bool
}

final class DetailsInteractor:DetailsInteractable {

    private var school:School!
    private let disposeBag = DisposeBag()
    
    var schoolStream: BehaviorRelay<School>
    var detailsDataSource = BehaviorRelay<[String?]>(value: [])
    
    private var networkService: SchoolDataServiceable
    
    private let takersString = "Total number of takers: "
    private let readingScore = "Critical reading average score: "
    private let mathScore = "Math average score: "
    private let writingScore = "Writing average score: "
    
    init(networkService: SchoolDataServiceable, school:School) {
        self.school = school
        self.networkService = networkService
        self.schoolStream = BehaviorRelay<School>(value: school)
        
        networkService.fetchGPA(for: school)
            .map({ (elements) -> School in
                self.school.gpa = elements.first
                return self.school
            })
            .subscribe(onNext: { school in
                self.schoolStream.accept(school)
            })
            .disposed(by: disposeBag)
    }
    
    func currentMenuItem( item:DetailsViewMenu){
        switch item {
            case .description:
                detailsDataSource.accept([school.description])
            case .location:
                let address = String(format:"%@\n%@, %@ %@",
                                     school.primaryAddressLine1 ?? "",
                                     school.city  ?? "", "NY",
                                     school.zip ?? "")
                let website = String(format:"%@", school.website ?? "")
                let email = String(format:"email: %@", school.schoolEmail ?? "N/A")
                let phoneNumber = String(format:"phone: %@", school.phoneNumber ?? "N/A")
                
                if school.schoolEmail != nil {
                    detailsDataSource.accept([address,
                                              website,
                                              email,
                                              phoneNumber])
                } else {
                    detailsDataSource.accept([address,
                                              website,
                                              phoneNumber])
                }
            case .gpa:
                guard let gpa = school.gpa else { return detailsDataSource.accept([]) }
                let takers = takersString + (gpa.numOfSatTestTakers ?? "N/A")
                let readingScoreString = readingScore + (gpa.satCriticalReadingAvgScore  ?? "N/A")
                let mathScoreString = mathScore + (gpa.satMathAvgScore ?? "N/A")
                let writingScoreString = writingScore + (gpa.satWritingAvgScore ?? "N/A")
                detailsDataSource.accept([takers,
                                      readingScoreString,
                                      mathScoreString,
                                      writingScoreString])
        }
    }
    
    func getSchoolLocationCoordinates() -> CLLocationCoordinate2D?{
        guard let latitudeString = school.latitude,
            let longitudeString = school.longitude,
            let latitude = Double(latitudeString),
            let longitude = Double(longitudeString) else {
                return nil
        }
        
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    func getSchoolName() -> String?{
        return school.schoolName
    }
    
    func hasLocationData() -> Bool{
        return school.latitude != nil && school.longitude != nil
    }
}
