//
//  SchoolsInteractor.swift
//  20200612-MikhailKouznetsov-NYCSchools
//
//  Created by Mikhail Kouznetsov on 6/12/20.
//  Copyright © 2020 Mikhail Kouznetsov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol SchoolsInteractable {
    var schools: BehaviorRelay<[School]> { get set }
    func getSchoolData()
}

final class SchoolsInteractor:SchoolsInteractable {
    private let disposeBag = DisposeBag()
    private var networkService: SchoolDataServiceable
    private let offset:Int = 20
    private var starting = 0
    private var isFinalLoad = false
    
    var schools = BehaviorRelay<[School]>(value: [])
    
    init( networkService:SchoolDataServiceable) {
        self.networkService = networkService
    }

    func getSchoolData(){
        if !isFinalLoad{
            networkService
                .fetchSchools(from: starting, to: starting + offset)
                .subscribe(onNext: { schools in
                    if schools.count < self.offset { self.isFinalLoad = true }
                    self.starting += schools.count
                    self.schools.accept(self.schools.value + schools)
                }).disposed(by: disposeBag)
        }
    }
}
