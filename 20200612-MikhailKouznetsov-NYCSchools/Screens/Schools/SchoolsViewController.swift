//
//  SchoolsViewController.swift
//  20200612-MikhailKouznetsov-NYCSchools
//
//  Created by Mikhail Kouznetsov on 6/12/20.
//  Copyright © 2020 Mikhail Kouznetsov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class SchoolsViewController: UIViewController {
    
    private let disposeBag = DisposeBag()
    private var interactor: SchoolsInteractable

    private let indicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(frame: CGRect(x:0, y:0, width:60, height:60))
        indicator.style = UIActivityIndicatorView.Style.medium
        return indicator
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: UITableView.Style.plain)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.backgroundColor = .systemBackground
        return tableView
    }()
    
    init(interactor: SchoolsInteractable) {
        self.interactor = interactor
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        setupBinding()
    }
    
    private func setupNavigation(){
        navigationItem.title = "New York Schools"
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    private func setupLayout(){
        tableView.frame = view.bounds
        view.addSubview(tableView)
        
        indicator.center = tableView.center
        view.addSubview(indicator)
    }
    
    private func setupBinding(){
        interactor
            .schools
            .asObservable()
            .observeOn(MainScheduler.instance)
            .bind(to: tableView.rx.items(cellIdentifier: "Cell", cellType: UITableViewCell.self)){ index, school, cell in
                cell.selectionStyle = .none
                cell.textLabel?.numberOfLines = 0
                cell.textLabel?.text = school.schoolName

                if (index + 3) == self.interactor.schools.value.count{
                    self.interactor.getSchoolData()
                }
            }.disposed(by: disposeBag)
        
        tableView.rx
            .modelSelected(School.self)
            .subscribe(onNext: { school in
                self.navigationItem.title = ""
                self.navigationController?.pushViewController( DetailsViewController(with: school), animated: true)
            }).disposed(by: disposeBag)
        
        interactor.getSchoolData()
    }
}
