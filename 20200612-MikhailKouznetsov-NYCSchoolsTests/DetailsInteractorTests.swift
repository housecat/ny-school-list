//
//  _0200612_MikhailKouznetsov_NYCSchoolsTests.swift
//  20200612-MikhailKouznetsov-NYCSchoolsTests
//
//  Created by Mikhail Kouznetsov on 6/12/20.
//  Copyright © 2020 Mikhail Kouznetsov. All rights reserved.
//

import XCTest
import RxSwift
@testable import _0200612_MikhailKouznetsov_NYCSchools

class DetailsInteractorTests: XCTestCase {
    let mockNetworkService = SchoolDataService()
    let school = School(dbn: nil,
                        schoolName: "Test School",
                        description: nil,
                        primaryAddressLine1: nil,
                        city: nil,
                        zip: nil,
                        stateCode: nil,
                        latitude: "0.00",
                        longitude: "0.00",
                        website: nil,
                        schoolEmail: nil,
                        phoneNumber: nil,
                        gpa: nil)

    func test_school_name() {
        let detailsInteractor = DetailsInteractor(networkService: mockNetworkService, school: school)
        XCTAssertEqual(detailsInteractor.getSchoolName()!, "Test School")
    }
    
    func test_school_location() {
        let detailsInteractor = DetailsInteractor(networkService: mockNetworkService, school: school)
        XCTAssertTrue(detailsInteractor.hasLocationData())
    }
}
